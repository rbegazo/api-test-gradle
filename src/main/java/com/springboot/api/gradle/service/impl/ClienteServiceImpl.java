package com.springboot.api.gradle.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.gradle.bean.Response;
import com.springboot.api.gradle.bean.ResponseCliente;
import com.springboot.api.gradle.bean.ResponseClientes;
import com.springboot.api.gradle.dao.impl.ClienteDaoImpl;
import com.springboot.api.gradle.model.Cliente;
import com.springboot.api.gradle.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ClienteDaoImpl _clienteDao;
	
	@Override
	public ResponseClientes getAllClientes() {
		List<Cliente> clientes = _clienteDao.getAllClientes();
		return respuestaClientesResult("0000", clientes);
	}

	@Override
	public ResponseCliente getCliente(Integer id) {
		Cliente  cliente = _clienteDao.getCliente(id);
		return respuestaClienteResult("0000", cliente);
	}

	@Override
	public Response saveCliente(Cliente cliente) {
		Response result = new Response();  
		try {
			_clienteDao.saveCliente(cliente);			
			result = respuestaResult("0000","Cliente\n" + 
					"registrado con éxito");
		} catch (Exception e) {
			logger.error(e.getMessage());
			result = respuestaResult("6666",e.getMessage());
		}
		return result;

	}

	@Override
	public Response deleteCliente(Integer id) {
		Response result = new Response(); 
		try {
			_clienteDao.deleteCliente(id);
			result = respuestaResult("0000","Cliente\n" + 
					"eliminado con éxito");
		} catch (Exception e) {
			logger.error(e.getMessage());
			result = respuestaResult("6666",e.getMessage());
		}
		return result;

	}
	
	private Response respuestaResult(String codigo,String descripcion) {
		Response respuesta = new Response();
		respuesta.setCodigo_servicio(codigo);
		respuesta.setDescripcion(descripcion);
		return respuesta;
	}
	
	private ResponseClientes respuestaClientesResult (String codigo, List<Cliente> clientes) {
		ResponseClientes respuesta = new ResponseClientes();
		respuesta.setClientes(clientes);
		respuesta.setCodigo_servicio(codigo);
		return respuesta;		
	}
	
	private ResponseCliente respuestaClienteResult (String codigo, Cliente cliente) {
		ResponseCliente respuesta = new ResponseCliente();
		respuesta.setCliente(cliente);
		respuesta.setCodigo_servicio(codigo);
		return respuesta;		
	}

}
