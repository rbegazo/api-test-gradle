package com.springboot.api.gradle.service;

import java.util.List;

import com.springboot.api.gradle.bean.Response;
import com.springboot.api.gradle.bean.ResponseCliente;
import com.springboot.api.gradle.bean.ResponseClientes;
import com.springboot.api.gradle.model.Cliente;

public interface ClienteService {
	ResponseClientes getAllClientes();
	ResponseCliente getCliente(Integer id);
	Response saveCliente(Cliente cliente);
	Response deleteCliente(Integer id);
}
