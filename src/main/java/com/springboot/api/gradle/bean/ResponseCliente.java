package com.springboot.api.gradle.bean;

import java.util.List;

import com.springboot.api.gradle.model.Cliente;

public class ResponseCliente {
	private String codigo_servicio;
	private Cliente cliente;

	public ResponseCliente() {

	}

	public String getCodigo_servicio() {
		return codigo_servicio;
	}

	public void setCodigo_servicio(String codigo_servicio) {
		this.codigo_servicio = codigo_servicio;
	}

	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


}
