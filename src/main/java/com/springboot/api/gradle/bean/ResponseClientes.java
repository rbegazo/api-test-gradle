package com.springboot.api.gradle.bean;

import java.util.List;

import com.springboot.api.gradle.model.Cliente;

public class ResponseClientes {
	private String codigo_servicio;
	private List<Cliente> clientes;

	public ResponseClientes() {

	}

	public String getCodigo_servicio() {
		return codigo_servicio;
	}

	public void setCodigo_servicio(String codigo_servicio) {
		this.codigo_servicio = codigo_servicio;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}


}
